mkdir -p $HOME/logs
mkdir -p $HOME/sources/bat
# curl -SL https://github.com/sharkdp/bat/releases > $HOME/logs/releases-bat.log
# cat $HOME/logs/releases-bat.log | grep 'deb'
[ -f $HOME/logs/releases-bat.log ] && echo '~/logs/releases-bat.log already exists...' || curl -SL https://github.com/sharkdp/bat/releases > $HOME/logs/releases-bat.log 
[ -f $HOME/sources/bat.tar.gz ] && echo '~/sources/bat.tar.gz already exists...'|| cat $HOME/logs/releases-bat.log |\
	grep linux |\
       	head -n 1 |\
       	cut -d'"' -f2 |\
        xargs -I % curl -SL https://github.com% -o $HOME/sources/bat.tar.gz
[ -f $HOME/sources/bat/bat ] && echo '~/sources/bat already exists...' || tar xvf $HOME/sources/bat.tar.gz -C $HOME/sources/bat
