mkdir -p $HOME/logs
mkdir -p $HOME/sources/fd
# curl -SL https://github.com/sharkdp/fd/releases > $HOME/logs/releases-fd.log
# cat $HOME/logs/releases-fd.log | grep 'deb'
[ -f $HOME/logs/releases-fd.log ] && echo '~/logs/releases-fd.log already exists...' || curl -SL https://github.com/sharkdp/fd/releases > $HOME/logs/releases-fd.log 
[ -f $HOME/sources/fd.tar.gz ] && echo '~/sources/fd.tar.gz already exists...'|| cat $HOME/logs/releases-fd.log |\
	grep linux |\
       	head -n 1 |\
       	cut -d'"' -f2 |\
        xargs -I % curl -SL https://github.com% -o $HOME/sources/fd.tar.gz
[ -f $HOME/sources/fd/fd ] && echo '~/sources/fd already exists...' || tar xvf $HOME/sources/fd.tar.gz -C $HOME/sources/fd
